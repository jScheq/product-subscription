export enum Currency {
  USD = 'USD'
};

export enum CurrencyDisplayName {
  USD = '$'
};
