export enum PaymentInterval {
  YEAR = 'YEAR',
  MONTH = 'MONTH',
};

export enum PaymentIntervalDisplayName {
  YEAR = 'year',
  MONTH = 'month',
};
