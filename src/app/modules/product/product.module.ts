import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductsService } from './services/products.service';
import { ProductResolver } from './resolvers/product.resolver';
import { ProductRoutingModule } from './product-routing.module';
import { ProductsStateService } from './services/products-state.service';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { SubscriptionComponent } from './components/pages/subscription/subscription.component';
import { ProductDetailsComponent } from './components/dialogs/product-details/product-details.component';


@NgModule({
  declarations: [
    SubscriptionComponent,
    ProductCardComponent,
    ProductDetailsComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    HttpClientModule,
    ProductRoutingModule,
  ],
  providers: [
    ProductsService,
    ProductResolver,
    ProductsStateService
  ]
})
export class ProductModule { }
