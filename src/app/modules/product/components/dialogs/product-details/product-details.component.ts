import { Component, Inject } from '@angular/core';
import { Product } from './../../../interfaces/product';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CurrencyDisplayName } from 'src/app/core/enums/currency';
import { PaymentIntervalDisplayName } from 'src/app/core/enums/payment-interval';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent {
  CurrencyDisplayName = CurrencyDisplayName;
  PaymentIntervalDisplayName = PaymentIntervalDisplayName;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public readonly data: { product: Product }
  ) {}
}
