import { Observable } from 'rxjs';
import { Product } from './../../../interfaces/product';
import { SubscriptionFacade } from './subscription.facade';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { productsProvider, productsErrorProvider, PRODUCTS_TOKEN, PRODUCTS_ERROR_TOKEN } from './products.provider';


@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss'],
  providers: [
    productsProvider,
    SubscriptionFacade,
    productsErrorProvider
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubscriptionComponent {
  constructor(
    private readonly facade: SubscriptionFacade,
    @Inject(PRODUCTS_TOKEN) public readonly products$: Observable<Product[]>,
    @Inject(PRODUCTS_ERROR_TOKEN) public readonly error$: Observable<string>,
  ) {}

  openProductDetailsDialog(product: Product): void {
    this.facade.openProductDetailsDialog(product);
  }
}
