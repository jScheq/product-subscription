import { Observable } from "rxjs";
import { Product } from "../../../interfaces/product";
import { InjectionToken, Provider } from "@angular/core";
import { ProductsStateService } from './../../../services/products-state.service';


export const PRODUCTS_TOKEN: InjectionToken<Observable<Product[]>> = new InjectionToken<Observable<Product[]>>(
  'A stream with products data'
);

export const PRODUCTS_ERROR_TOKEN: InjectionToken<Observable<string>> = new InjectionToken<Observable<string>>(
  'A stream with products data'
)

export const productsProvider: Provider = {
  provide: PRODUCTS_TOKEN,
  useFactory: (productsStateService: ProductsStateService) => productsStateService.products$,
  deps: [ProductsStateService]
};

export const productsErrorProvider: Provider = {
  provide: PRODUCTS_ERROR_TOKEN,
  useFactory: (productsStateService: ProductsStateService) => productsStateService.error$,
  deps: [ProductsStateService]
};
