
import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Product } from './../../../interfaces/product';
import { ProductDetailsComponent } from '../../dialogs/product-details/product-details.component';

@Injectable()
export class SubscriptionFacade {
  constructor(
    private readonly dialog: MatDialog
  ) {}

  openProductDetailsDialog(product: Product): void {
    this.dialog.open(
      ProductDetailsComponent,
      {
        data: {
          product
        }
      }
    );
  }
}