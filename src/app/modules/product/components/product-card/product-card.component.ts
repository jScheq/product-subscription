import { Product } from './../../interfaces/product';
import { CurrencyDisplayName } from 'src/app/core/enums/currency';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PaymentIntervalDisplayName } from 'src/app/core/enums/payment-interval';


@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent {

  @Input() product!: Product;
  @Output() onPlanSelect: EventEmitter<void> = new EventEmitter<void>();

  CurrencyDisplayName = CurrencyDisplayName;
  PaymentIntervalDisplayName = PaymentIntervalDisplayName;

  selectPlan(): void {
    this.onPlanSelect.emit();
  }
}
