import { Product } from '../interfaces/product';
import { ProductPlan } from './../interfaces/product';
import { BehaviorSubject, Observable, Subject } from 'rxjs';


export class ProductsStateService {
  private readonly _error$: Subject<string> = new Subject<string>();
  private readonly _products$: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);

  get products$(): Observable<Product[]> {
    return this._products$.asObservable();
  }

  get error$(): Observable<string> {
    return this._error$.asObservable();
  }

  setProducts(products: Product[]): void {
    this._products$.next(
      products.map(
        (product: Product) => ({
          ...product,
          plans: product.plans.map((plan: ProductPlan) => ({
            ...plan,
            paymentInterval: plan.paymentInterval 
              ? plan.paymentInterval
              : plan.interval
          }))
        })
      )
    );
  }

  setError(error: string): void {
    this._error$.next(error);
  }
}
