import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { Injectable } from '@angular/core';
import { Product } from '../interfaces/product';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../environments/environment';
import { ProductsResponsePayload } from '../interfaces/products-response';


@Injectable()
export class ProductsService {
  constructor(
    private readonly httpClient: HttpClient
  ) {}

  getProducts(): Observable<Product[]> {
    return this.httpClient.get<ProductsResponsePayload>(
      `${environment.productSubscriptionServiceUrl}/products`
    ).pipe(
      map((res: ProductsResponsePayload) => res.products)
    );
  }
}
