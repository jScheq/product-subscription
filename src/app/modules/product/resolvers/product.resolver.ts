import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Product } from '../interfaces/product';
import { catchError, first, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { ProductsService } from './../services/products.service';
import { ProductsStateService } from '../services/products-state.service';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';


@Injectable()
export class ProductResolver implements Resolve<boolean> {
  constructor(
    private readonly productService: ProductsService,
    private readonly productStateService: ProductsStateService,
  ) {

  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.productService.getProducts().pipe(
      first(),
      tap((products: Product[]) => this.productStateService.setProducts(products)),
      catchError(({ message }: HttpErrorResponse) => {
        this.productStateService.setError(message);
        return of(message);
      })
    ).subscribe()

    return of(true);
  }
}
