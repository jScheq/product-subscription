import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductResolver } from './resolvers/product.resolver';
import { SubscriptionComponent } from './components/pages/subscription/subscription.component';


const routes: Routes = [
  { 
    path: 'subscription',
    resolve: {
      products: ProductResolver
    },
    component: SubscriptionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
