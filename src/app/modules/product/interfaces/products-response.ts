import { Product } from "./product";


export interface ProductsResponsePayload {
  products: Product[];
}
