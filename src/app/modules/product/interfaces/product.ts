import { Currency } from "src/app/core/enums/currency";
import { PaymentInterval } from "src/app/core/enums/payment-interval";


export interface Product {
  id: string;
  name: string;
  features: string[];
  plans: ProductPlan[];
}

export interface ProductPlan {
  id: string;
  priceCents: number;
  currency: Currency;
  interval: PaymentInterval;
  paymentInterval: PaymentInterval;
}