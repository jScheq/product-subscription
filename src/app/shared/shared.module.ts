import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './modules/material/material.module';
import { PreloaderComponent } from './components/preloader/preloader.component';



@NgModule({
  declarations: [
    PreloaderComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    MaterialModule,
    PreloaderComponent
  ]
})
export class SharedModule { }
