export const environment = {
  production: true,
  productSubscriptionServiceUrl: 'https://product-subscription.herokuapp.com/api'
};
